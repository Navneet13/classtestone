import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BlazeDemoTest {
	
	//Location of chromedriver file
    final String CHROMEDRIVER_LOCATION = "/Users/navpreetkaur/Desktop/chromedriver";
   
    //Website we wwant to test
    final String URL_TO_TEST = "http://blazedemo.com";
    
    //Global Driver declaration
	WebDriver driver;		


	@Before
	public void setUp() throws Exception {
		//Setup Selenium

		System.setProperty("webdriver.chrome.driver",CHROMEDRIVER_LOCATION);
				
		driver = new ChromeDriver();
				
		//Go to website
		driver.get(URL_TO_TEST);
				
	}

	@After
	public void tearDown() throws Exception {
		// Close the browser
			Thread.sleep(2000);
			driver.close();
	}

	//TC1: There are 7 departures Cities
	@Test
	public void testCase1() {
		// Get the access to the list
		List<WebElement> departureList = driver.findElements(By.name("fromPort"));//.options.length);
		System.out.println(departureList.size());
		int count = 0;
		for(int i=0; i < departureList.size(); i++)
		{    
		    count++;
		}
		
		
		
		//Checking of number of departure cities is 7
		
		assertEquals(7, count);
	}
	
	//TC2: Virgin America flight #12 Details
	@Test
	public void testCase2() {
	//get the access to the table row
		WebElement virginAmerica = driver.findElement(By.name("VA12"));
		
		
}
	
	//TC3: TESTING THE PURCHASE CONFIRMATION PAGE
	
	@Test
	public void testCase3() {
	//1. WEBSITE GENERATE RANDOM 13 DIGIT ID NUMBER FOR THE TRANSACTION
		
	//2. THE DATE OF PURCHASE MATCHES TODAY'S DATE
		
}
	
}
