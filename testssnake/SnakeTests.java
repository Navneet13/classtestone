import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTests {
	
	Snake snake1,snake2;
	
	@Before
	public void setUp() throws Exception {
		snake1 = new Snake("Peter",10,"coffee");
		snake2 = new Snake("Takis",80,"vegetables");
	}

	@After
	public void tearDown() throws Exception {
		
	}

	//TC1: Testing the isHealthy() function
	@Test
	public void testForHealthySnake() {
		
		//TC1    (a) Check if snake is unhealthy
		
		String expectedResultA = "Unhealthy";
		String actualResultA;
		
		//Test Logic
	    
	    boolean healthStatusA = snake1.isHealthy();
	    if (healthStatusA == false) {
	    	actualResultA  = "Unhealthy";
	    }
	    else
	    	actualResultA = "Healthy";
	    	
	   //Testing...
		assertEquals(expectedResultA,actualResultA);
		
		
		
	   //TC1: (b) Check if snake is healthy
		    String expectedResultB = "Healthy";
			String actualResultB;
			
			//Test Logic
		    
		    boolean healthStatusB = snake2.isHealthy();
		    if (healthStatusB == true) {
		    	actualResultB  = "Healthy";
		    }
		    else
		    	actualResultB = "Unhealthy";
		    	
		   //Testing...
			assertEquals(expectedResultB,actualResultB);
			
		}
	
	
	     //TC2: Testing the fitsInCage() function
	
	     @Test
         public void testIfSnakeFitsInCage() {
	    	         int cageLength;
	    	         boolean expectedResult,actualResult;
	    	         
		
	    	// (a) Check If CageLength < SnakeLength
	    	 
	    	         cageLength = 5;
	    	         
	    	         expectedResult = false;
	    	         actualResult = snake1.fitsInCage(cageLength);
	    	         
	    	         //Testing...
	    	         assertEquals(expectedResult,actualResult);
	    	   
	    	      
	    	 
	    	// (b) If CageLength = SnakeLength
	    	         
                     cageLength = 10;
                     expectedResult = false;
                     actualResult = snake1.fitsInCage(cageLength);
	    	        
	    	         //Testing...
	    	         assertEquals(expectedResult,actualResult);
	    	         
	    	 
	    	//(C) If CageLength > SnakeLength
	    	 
	    	         cageLength = 90;
                     expectedResult = true;
                     actualResult = snake2.fitsInCage(cageLength);
	    	        
	    	         //Testing...
	    	         assertEquals(expectedResult,actualResult);
	    	  
	     }
	
	
}
